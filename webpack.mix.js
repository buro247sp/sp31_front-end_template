let mix = require('laravel-mix');

mix.options({
    processCssUrls: false,
    publicPath: 'public'
});

mix.js('resources/assets/js/app.js', 'public/js/scripts.js')
   .sass('resources/assets/scss/app.scss', 'public/css/style.css');
